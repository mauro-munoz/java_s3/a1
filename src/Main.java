import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        HashMap<String,Integer> stock = new HashMap<>();
        stock.put("Mario Odyssey",50);
        stock.put("Super Smash Bros. Ultimate",20);
        stock.put("Luigi's Mansion",15);
        stock.put("Pokemon Sword",30);
        stock.put("Pokemon Shield",100);

        stock.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stock.");
        });


        ArrayList<String> topGames = new ArrayList<>();
        stock.forEach((key,value)-> {
            if (value <= 30){
                topGames.add(key);
                System.out.println(key + " has been added to Top Games.");
            }
        });
        System.out.println("Our shops top games are: "+ topGames);
    }
}